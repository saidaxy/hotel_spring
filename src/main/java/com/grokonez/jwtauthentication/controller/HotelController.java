package com.grokonez.jwtauthentication.controller;

import com.grokonez.jwtauthentication.exception.ResourceNotFoundException;
import com.grokonez.jwtauthentication.model.Hotel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.grokonez.jwtauthentication.repository.HotelRepository;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("api/auth")
public class HotelController {
    @Autowired

    private HotelRepository hotelRepository;
    @GetMapping("/hotels")

    public List<Hotel> getAllHotels() {
        return hotelRepository.findAll();

    }



    @GetMapping("/hotels/{id}")
    public ResponseEntity<Hotel> getHotelById(@PathVariable(value = "id") Long id)

            throws ResourceNotFoundException {

        Hotel hotel = hotelRepository.findById(id)

                .orElseThrow(() -> new ResourceNotFoundException("Hotel not found for this id :: " + id));

        return ResponseEntity.ok().body(hotel);

    }



    @PostMapping("/hotels")

    public Hotel createHotel(@Valid @RequestBody Hotel hotel) {

        return hotelRepository.save(hotel);

    }



    @PutMapping("/hotels/{id}")

    public ResponseEntity<Hotel> updateHotel(@PathVariable(value = "id") Long id,

                                             @Valid @RequestBody Hotel hotelDetails) throws ResourceNotFoundException {

        Hotel hotel = hotelRepository.findById(id)

                .orElseThrow(() -> new ResourceNotFoundException("Hotel not found for this id :: " + id));



        hotel.setId(hotelDetails.getId());

        hotel.setName(hotelDetails.getName());

        hotel.setCity(hotelDetails.getCity());

        hotel.setStreet(hotelDetails.getStreet());

        hotel.setDescription(hotelDetails.getDescription());

        hotel.setPrice(hotelDetails.getPrice());

        final Hotel updatedHotel = hotelRepository.save(hotel);

        return ResponseEntity.ok(updatedHotel);

    }



    @DeleteMapping("/hotels/{id}")

    public Map<String, Boolean> deleteHotel(@PathVariable(value = "id") Long id)

            throws ResourceNotFoundException {

        Hotel hotel = hotelRepository.findById(id)

                .orElseThrow(() -> new ResourceNotFoundException("Employee not found for this id :: " + id));



        hotelRepository.delete(hotel);

        Map<String, Boolean> response = new HashMap<>();

        response.put("deleted", Boolean.TRUE);

        return response;

    }

}

