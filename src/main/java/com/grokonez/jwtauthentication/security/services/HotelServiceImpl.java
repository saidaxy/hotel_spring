package com.grokonez.jwtauthentication.security.services;

import com.grokonez.jwtauthentication.model.Hotel;
import com.grokonez.jwtauthentication.model.HotelDto;
import com.grokonez.jwtauthentication.repository.HotelRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service(value = "hotelService")
public class HotelServiceImpl implements HotelService {

    @Autowired
    private HotelRepository hotelDao;


    public List<Hotel> findAll() {
        List<Hotel> list = new ArrayList<>();
        hotelDao.findAll().iterator().forEachRemaining(list::add);
        return list;
    }

    @Override
    public void delete(Long id) {
        hotelDao.deleteById(id);
    }

    @Override
    public Hotel findOne(String title) {
        return hotelDao.findByName(title);
    }

    @Override
    public Hotel findById(Long id) {
        Optional<Hotel> optionalHotel = hotelDao.findById(id);
        return optionalHotel.isPresent() ? optionalHotel.get() : null;
    }

    @Override
    public HotelDto update(HotelDto hotelDto) {
        Hotel hotel = findById(hotelDto.getId());
        if(hotel != null) {
            BeanUtils.copyProperties(hotelDto, hotel);
            hotelDao.save(hotel);
        }
        return hotelDto;
    }

    @Override
    public Hotel save(HotelDto hotel) {
        Hotel newHotel = new Hotel();
        newHotel.setName(hotel.getName());
        newHotel.setCity(hotel.getCity());
        newHotel.setStreet(hotel.getStreet());
        newHotel.setDescription(hotel.getDescription());
        return hotelDao.save(newHotel);
    }


}
