package com.grokonez.jwtauthentication.security.services;

import com.grokonez.jwtauthentication.model.Hotel;
import com.grokonez.jwtauthentication.model.HotelDto;

import java.util.List;

public interface HotelService {
    Hotel save(HotelDto hotel);
    List<Hotel> findAll();
    void delete(Long id);

    Hotel findOne(String name);

    Hotel findById(Long id);

    HotelDto update(HotelDto hotelDto);
}
